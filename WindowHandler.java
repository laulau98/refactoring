import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * 
 * @author laurence.sylva
 *
 */
public class WindowHandler extends WindowAdapter {

    HardwareStore h;

    public WindowHandler(HardwareStore s) {
        h = s;
    }

    public void windowClosing( WindowEvent e ) { h.cleanup(); }

}
