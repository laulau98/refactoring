/**
 * Classe stockant les donn�es de Drill press
 * @author laurence.sylva
 *
 */
public class DrillPressMock {

    public String drillPress[] = {
            { "0", "Kraftsman Professional 15 in. Drill Press with Variable Speed Control", "Kraftsman",
                    "Kraftsman Professional 15 in. Drill Press with Variable Speed Control", "22935", "7", "1,249.99" },
            { "1", "Kraftsman 20 in. Drill Press, 3/4 in. Chuck, Stationary", "Kraftsman",
                    "Kraftsman 20 in. Drill Press, 3/4 in. Chuck, Stationary", "22920", "5", "649.99" },
            { "2", "Kraftsman 20 in. Drill Press", "Kraftsman", "Kraftsman 20 in. Drill Press", "OR20601", "9",
                    "599.99" },
            { "3", "Kraftsman 17 in. Drill Press", "Kraftsman", "Kraftsman 17 in. Drill Press", "OR20501", "8",
                    "529.99" },
            { "4", "Kraftsman 15 in. Drill Press", "Kraftsman", "Kraftsman 15 in. Drill Press", "OR20451", "7",
                    "319.99" },
            { "5", "Kraftsman Laser Trac� 15 in. Drill Press", "Kraftsman", "Kraftsman Laser Trac� 15 in. Drill Press",
                    "229250", "11", "269.88" },
            { "6", "Kraftsman Hollow Chisel Mortiser", "Kraftsman", "Kraftsman Hollow Chisel Mortiser", "OR25101", "7",
                    "199.99" },
            { "7", "Kraftsman 10 in. Drill Press with Laser", "Kraftsman", "Kraftsman 10 in. Drill Press with Laser",
                    "21900", "7", "99.99" },
            { "8", "Kraftsman 9 in. Drill Press", "Kraftsman", "Kraftsman 9 in. Drill Press", "48030", "7", "89.88" },
            { "9", "Dompanion 8 in. Drill Press", "Dompanion", "Dompanion 8 in. Drill Press", "21499", "5", "49.97" },
            { "10", "", "", "", "", "", "" }, { "11", "", "", "", "", "", "" }, { "12", "", "", "", "", "", "" },
            { "13", "", "", "", "", "", "" }, { "14", "", "", "", "", "", "" }, { "15", "", "", "", "", "", "" },
            { "16", "", "", "", "", "", "" }, { "17", "", "", "", "", "", "" }, { "18", "", "", "", "", "", "" },
            { "19", "", "", "", "", "", "" }, { "20", "", "", "", "", "", "" }, { "21", "", "", "", "", "", "" },
            { "22", "", "", "", "", "", "" }, { "23", "", "", "", "", "", "" } };
    
}
