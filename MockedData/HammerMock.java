/**
 * <b>Classe stockant les donn�es de Hammer </b>
 * @author laurence.sylva
 *
 */
public class HammerMock {

    public String hammer[]= {
            { "0", "Kraftsman 5 pc. Ball Pein Hammer Set", "Kraftsman", "Kraftsman 5 pc. Ball Pein Hammer Set",
                    "9-38528", "11", "89.99" },
            { "1", "Kraftsman Dead Blow Hammer Set, 3 pc.", "Kraftsman", "Kraftsman Dead Blow Hammer Set, 3 pc.",
                    "38358", "7", "79.99" },
            { "2", "Kraftsman 3 pc. Steel Handle Ball Pein Hand Driller Set", "Kraftsman",
                    "Kraftsman 3 pc. Steel Handle Ball Pein Hand Driller Set", "38529", "9", "59.99" },
            { "3", "Nuqula 2 pc. Hammer Set", "Nuqula", "Nuqula 2 pc. Hammer Set", "6030208", "11", "59.99" },
            { "4", "Kraftsman 2 pc. Dead Blow Hammer Set", "Kraftsman", "Kraftsman 2 pc. Dead Blow Hammer Set",
                    "6030205", "7", "59.99" },
            { "5", "Kraftsman 5 pc. Hammer Set", "Kraftsman", "Kraftsman 5 pc. Hammer Set", "38074", "6", "59.99" },
            { "6", "Nuqula 2 pc. Hammer Set", "Nuqula", "Nuqula 2 pc. Hammer Set", "6030207", "7", "49.99" },
            { "7", "Westwing 21 oz. Fiberglass Hammer, 16 in. Handle", "Westwing",
                    "Westwing 21 oz. Fiberglass Hammer, 16 in. Handle", "WF21LM", "11", "34.99" },
            { "9", "Westwing 28 oz. Framing Hammer", "Westwing", "Westwing 28 oz. Framing Hammer", "W3-28SM", "6",
                    "31.99" },
            { "10", "Kraftsman 1-1/4 lb. Camp Axe", "Kraftsman", "Kraftsman 1-1/4 lb. Camp Axe", "4810", "7", "29.99" },
            { "11", "Nuqula 3 lb. Dead Blow Hammer", "Nuqula", "Nuqula 3 lb. Dead Blow Hammer", "SF-35G,10-035", "9",
                    "29.99" },
            { "12", "Westwing 17 oz. Fiberglass Hammer, 16 in. Handle", "Westwing",
                    "Westwing 17 oz. Fiberglass Hammer, 16 in. Handle", "WF17L", "9", "29.99" },
            { "13", "Kraftsman 2 lb. Hammer, Power Drive�", "Kraftsman", "Kraftsman 2 lb. Hammer, Power Drive�",
                    "SDSF2SG", "8", "29.99" },
            { "14", "Westwing 21 oz. Fiberglass Hammer, 14 in. Handle", "Westwing",
                    "Westwing 21 oz. Fiberglass Hammer, 14 in. Handle", "WF21", "7", "29.99" },
            { "15", "", "", "", "", "", "" }, { "16", "", "", "", "", "", "" }, { "17", "", "", "", "", "", "" },
            { "18", "", "", "", "", "", "" }, { "19", "", "", "", "", "", "" }, { "20", "", "", "", "", "", "" },
            { "21", "", "", "", "", "", "" }, { "22", "", "", "", "", "", "" }, { "23", "", "", "", "", "", "" } };
    
}
