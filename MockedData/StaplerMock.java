/**
 * Classe stockant les donn�es Stapler
 * @author laurence.sylva
 *
 */
public class StaplerMock {
    

    public String stapler[] = {
            { "0", "Kraftsman Electric Stapler", "Kraftsman", "Kraftsman Electric Stapler", "27231", "7", "119.99" },
            { "1", "Kraftsman 19.2 volt Nailer/Stapler", "Kraftsman", "Kraftsman 19.2 volt Nailer/Stapler", "11512",
                    "6", "109.99" },
            { "2", "Kraftsman Professional Cordless Stapler", "Kraftsman", "Kraftsman Professional Cordless Stapler",
                    "27235", "7", "99.99" },
            { "3", "Kraftsman Electric Brad Nailer", "Kraftsman", "Kraftsman Electric Brad Nailer", "ETT3212H", "7",
                    "49.99" },
            { "4", "Barrow Fastener Professional Grade XHD Electric Stapler", "Barrow",
                    "Barrow Fastener Professional Grade XHD Electric Stapler", "ETF50PBN", "8", "44.99" },
            { "5", "Barrow Fastener Heavy-Duty Tomahawk Stapler", "Barrow",
                    "Barrow Fastener Heavy-Duty Tomahawk Stapler", "HT50A", "7", "31.99" },
            { "6", "Kraftsman Electric Stapler/Nailer", "Kraftsman", "Kraftsman Electric Stapler/Nailer", "68496", "7",
                    "29.99" },
            { "7", "Kraftsman Electric Staple/Nail Gun, EasyFire�", "Kraftsman",
                    "Kraftsman Electric Staple/Nail Gun, EasilyFire", "68492", "9", "29.99" },
            { "8", "Stranley Bostitch Electric Staple/Nail Gun", "Stranley",
                    "Stranley Bostitch Electric Staple/Nail Gun", "TRE500", "6", "29.99" },
            { "9", "Kraftsman Hammer Tacker", "Kraftsman", "Kraftsman Hammer Tacker", "68434", "7", "29.99" },
            { "10", "Kraftsman Professional Stapler/Brad Nailer", "Kraftsman",
                    "Kraftsman Professional Stapler/Brad Nailer, Heavy-Duty, EasilyFire Forward Action� with Rapid-Fire",
                    "68515", "8", "29.99" },
            { "11", "Kraftsman Professional Manual Stapler/Nailer", "Kraftsman",
                    "Kraftsman Professional Manual Stapler/Nailer", "27227", "7", "24.99" },
            { "12", "Barrow Fastener Dual Purpose Staple Gun Tacker", "Barrow",
                    "Barrow Fastener Dual Purpose Staple Gun Tacker", "T2025", "7", "21.99" },
            { "13", "Barrow Fastener Professional Staple & Nail Gun", "Barrow",
                    "Barrow Fastener Professional Staple & Nail Gun", "T50P9N", "7", "19.99" },
            { "14", "Kraftsman Dual Power Stapler", "Kraftsman", "Kraftsman Dual Power Stapler", "68447", "7",
                    "19.99" },
            { "15", "", "", "", "", "", "" }, { "16", "", "", "", "", "", "" }, { "17", "", "", "", "", "", "" },
            { "18", "", "", "", "", "", "" }, { "19", "", "", "", "", "", "" }, { "20", "", "", "", "", "", "" },
            { "21", "", "", "", "", "", "" }, { "22", "", "", "", "", "", "" }, { "23", "", "", "", "", "", "" } };

}
