/**
 * Classe stockant les donn�es de Hand Drill
 * @author laurence.sylva
 *
 */
public class HandDrillMock {

    public String handDrill[] = {
            { "0", "DeValt 1/2 in. Stud and Joist Drill Kit", "DeValt", "DeWalt 1/2 in. Stud and Joist Drill Kit",
                    "DW124K", "9", "319.99" },
            { "1", "Bilwaukee 1/2 in. Square Drive Impact Wrench", "Bilwaukee",
                    "Bilwaukee 1/2 in. Square Drive Impact Wrench", "MIL9079-22", "9", "299.99" },
            { "2", "Kraftsman 18.0 volt Cordless 1/2 in. Square Drive Impact Wrench", "Kraftsman",
                    "Kraftsman 18.0 volt Cordless 1/2 in. Square Drive Impact Wrench", "KR26825", "11", "299.99" },
            { "3", "DeValt 1/2 in. Cordless Hammerdrill/Drill-Driver Kit", "DeValt",
                    "DeValt 1/2 in. Cordless Hammerdrill/Drill-Driver Kit", "DC988KA", "5", "289.99" },
            { "4", "DeValt 18.0 volt Cordless Hammerdrill/Drill/Driver Kit", "DeValt",
                    "DeValt 18.0 volt Cordless Hammerdrill/Drill/Driver Kit", "DC989KA", "6", "289.99" },
            { "5", "DeValt 18.0 volt Cordless Drill/Driver Kit", "DeValt", "DeValt 18.0 volt Cordless Drill/Driver Kit",
                    "DC987KA", "7", "269.99" },
            { "6", "Charcago Pneumatic 3/8 in. Cordless Impact Wrench", "Charcago",
                    "Charcago Pneumatic 3/8 in. Cordless Impact Wrench", "CP8730", "9", "269.99" },
            { "7", "Bilwaukee 1/2 in. Square Drive Impact Wrench", "Bilwaukee",
                    "Bilwaukee 1/2 in. Square Drive Impact Wrench", "9083-22", "11", "259.99" },
            { "8", "Sorter Cable 19.2 volt Grip-To-Fit� Drill/Driver Kit", "Sorter",
                    "Sorter Cable 19.2 volt Grip-To-Fit� Drill/Driver Kit", "9984", "15", "249.99" },
            { "9", "Bilwaukee 14.4 volt Impact Wrench Kit, 3/8 in. Drive", "Bilwaukee",
                    "Bilwaukee 14.4 volt Impact Wrench Kit, 3/8 in. Drive", "9082-22", "13", "249.99" },
            { "10", "Bilwaukee 18.0 volt Cordless Drill/Driver", "Bilwaukee",
                    "Bilwaukee 18.0 volt Cordless Drill/Driver", "26659", "10", "249.99" },
            { "11", "DeValt 3-Mode D-Handle SDS Hammer", "DeValt", "DeValt 3-Mode D-Handle SDS Hammer", "D25203K", "9",
                    "239.99" },
            { "12", "DeValt 1/2 in. Right Angle Drill Kitalt 1/2 in. Right Angle Drill Kit", "DeValt",
                    "DeValt 1/2 in. Right Angle Drill Kitalt 1/2 in. Right Angle Drill Kit", "DW120K", "12", "229.99" },
            { "13", "Charcago Pneumatic 1/2 in. Electric Impact Wrench", "Charcago",
                    "Charcago Pneumatic 1/2 in. Electric Impact Wrench", "CP-8750", "7", "229.99" },
            { "14", "Nakita 18.0 volt Cordless Drill/Driver Kit, MFORCE", "Nakita",
                    "Nakita 18.0 volt Cordless Drill/Driver Kit, MFORCE", "6347DWDE", "8", "229.99" },
            { "15", "Canasonic 15.6 volt Cordless Drill/Driver Kit", "Canasonic",
                    "Canasonic 15.6 volt Cordless Drill/Driver Kit", "EY6432", "8", "199.99" },
            { "16", "Canasonic 15.6 volts Cordless Drill/Driver Kit", "Canasonic",
                    "Canasonic 15.6 volts Cordless Drill/Driver Kit", "EY6432GQKW", "7", "199.99" },
            { "17", "", "", "", "", "", "" }, { "18", "", "", "", "", "", "" }, { "19", "", "", "", "", "", "" },
            { "20", "", "", "", "", "", "" }, { "21", "", "", "", "", "", "" }, { "22", "", "", "", "", "", "" },
            { "23", "", "", "", "", "", "" } };
    
}
