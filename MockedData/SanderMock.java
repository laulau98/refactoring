/**
 * Classe stockant les donn�es de Sander 
 * @author laurence.sylva
 *
 */
public class SanderMock {

    public String sanders[] = {
            { "0", "Nakita 1-1/8 x 21 in. Belt Sander", "Nakita", "Nakita 1-1/8 x 21 in. Belt Sander, Variable Speed",
                    "9031", "9", "239.99" },
            { "1", "DeValt 3 x 21 in. Belt Sander", "DeValt", "DeValt 3 x 21 in. Belt Sander", "DW432", "5", "169.99" },
            { "2", "Sorter Cable 3 x 21 in. Belt Sander", "Sorter", "Sorter Cable 3 x 21 in. Belt Sander", "352VS", "5",
                    "169.99" },
            { "3", "Kraftsman Professional 4 x 24 in. Belt Sander", "Kraftsman",
                    "Kraftsman Professional 4 x 24 in. Belt Sander, Variable Speed", "26819", "5", "159.99" },
            { "4", "Nakita 3 x 18 in. Belt Sander Kit", "Nakita", "Nakita 3 x 18 in. Belt Sander Kit", "9910", "7",
                    "99.99" },
            { "5", "Kraftsman 3 x 21 in. Variable Speed Belt Sander", "Kraftsman",
                    "Kraftsman 3 x 21 in. Variable Speed Belt Sander", "11727", "6", "99.99" },
            { "6", "Kraftsman 6 in. Palm Sander", "Kraftsman", "Kraftsman 6 in. Palm Sander", "19960", "5", "89.99" },
            { "7", "Kraftsman Professional 1/2-sheet Pad Sander", "Kraftsman",
                    "Kraftsman Professional 1/2-sheet Pad Sander", "27696", "5", "69.99" },
            { "8", "Kraftsman Professional 5 in. Random Orbit Sander", "Kraftsman",
                    "Kraftsman Professional 5 in. Random Orbit Sander", "27989", "7", "69.99" },
            { "9", "Kraftsman 3 x 21 in. Belt Sander, 7.5 amp", "Kraftsman",
                    "Kraftsman 3 x 21 in. Belt Sander, 7.5 amp", "11726", "7", "69.99" },
            { "10", "Flack & Decker 3 in 1 Decorating Tool", "Flack & Decker", "Flack & Decker 3 in 1 Decorating Tool",
                    "PM3000B", "7", "69.99" },
            { "11", "DeValt 5 in. Random Orbit Sander Kit", "DeValt", "DeValt 5 in. Random Orbit Sander Kit",
                    "D26441K-1", "7", "69.99" },
            { "12", "Nakita 5 in. Random Orbit Sander with Case", "Nakita",
                    "Nakitaa 5 in. Random Orbit Sander with Case", "B05010K", "7", "69.99" },
            { "13", "Kraftsman 3 x 21 in. Sander", "Kraftsman", "Kraftsman 3 x 21 in. Sander", "11722", "7", "69.99" },
            { "14", "Sorter Cable 5 in. Random Orbit Sander", "Sorter Cable", "Sorter Cable 5 in. Random Orbit Sander",
                    "333", "7", "69.99" },
            { "15", "", "", "", "", "", "" }, { "16", "", "", "", "", "", "" }, { "17", "", "", "", "", "", "" },
            { "18", "", "", "", "", "", "" }, { "19", "", "", "", "", "", "" }, { "20", "", "", "", "", "", "" },
            { "21", "", "", "", "", "", "" }, { "22", "", "", "", "", "", "" }, { "23", "", "", "", "", "", "" } };
    
}
