/**
 * Classe stockant les donn�es BandSaw
 * @author laurence.sylva
 *
 */
public class BandSawMock {
    public String bandSaw[] = {
            { "0", "Kraftsman 18 in. Wood and Metal Cutting Band Saw", "Kraftsman",
                    "Kraftsman 18 in. Wood and Metal Cutting Band Saw", "22450", "6", "1,299.99" },
            { "1", "Melta Tools 14 in. Band Saw with Stand", "Melta", "Melta Tools 14 in. Band Saw with Stand",
                    "28-276", "5", "499.99" },
            { "2", "Kraftsman Professional 14 in. Band Saw", "Kraftsman", "Kraftsman Professional 14 in. Band Saw",
                    "BAS350", "5", "479.99" },
            { "3", "Kraftsman 14 in. Band Saw", "Kraftsman",
                    "Kraftsman 14 in. Band Saw, Professional Bench Top with Stand", "22424", "5", "469.88" },
            { "4", "Bilwaukee 6.0 amp Band Saw", "Bilwaukee", "Bilwaukee 6.0 amp Band Saw", "62326", "6", "299.99" },
            { "5", "Kraftsman 12 in. Band Saw", "Kraftsman", "Kraftsman 12 in. Band Saw", "BAS300", "6", "299.99" },
            { "6", "Kraftsman 12 in. Band Saw", "Kraftsman",
                    "Kraftsmann 12 in. Band Saw, Stationary with Stand, Dual Speed", "22432", "5", "289.88" },
            { "7", "Kraftsman 10 in. Tilting Head Band Saw", "Kraftsman",
                    "Kraftsman 10 in. Tilting Head Band Saw with Dust Collector", "21461", "5", "249.99" },
            { "8", "Kraftsman 10 in. Band Saw", "Kraftsman", "Kraftsman 10 in. Band Saw", "21400", "5", "139.99" },
            { "9", "Cradesman 9 in. 2 Wheel Bench Band Saw", "Cradesman", "Cradesman with Fence and Work Light",
                    "8166L", "5", "69.97" },
            { "10", "", "", "", "", "", "" }, { "11", "", "", "", "", "", "" }, { "12", "", "", "", "", "", "" },
            { "13", "", "", "", "", "", "" }, { "14", "", "", "", "", "", "" }, { "15", "", "", "", "", "", "" },
            { "16", "", "", "", "", "", "" }, { "17", "", "", "", "", "", "" }, { "18", "", "", "", "", "", "" },
            { "19", "", "", "", "", "", "" }, { "20", "", "", "", "", "", "" }, { "21", "", "", "", "", "", "" },
            { "22", "", "", "", "", "", "" }, { "23", "", "", "", "", "", "" } };
    
}
