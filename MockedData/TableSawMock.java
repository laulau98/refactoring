/**
 * Classe stockant les donn�es tableSaw
 * @author laurence.sylva
 *
 */
public class TableSawMock {

    public String tableSaw[] = { { "0", "Kraftsman Professional 12-inch 3-hp Table Saw", "Kraftsman",
            "Kraftsman Professional 12-inch 3-hp Table Saw with Wooden Extension, 2 Support Legs & Right-tilt Arbor",
            "22802", "5", "1,599.99" },
            { "1", "Kraftsman Professional 12-inch 3-hp Table Saw", "Kraftsman",
                    "Kraftsman Professional 12-inch 3-hp Table Saw with Wooden Extension & 2 Support Legs & Left Tilt Arbor Sears item ",
                    "22803", "5", "1,599.99" },
            { "2", "Kraftsman Professional 10-inch 3-hp Table Saw", "Kraftsman",
                    "Kraftsman Professional 10-inch 3-hp Table Saw with Wooden Extension & 2 Support Legs & Left Tilt Arbor",
                    "22805", "5", "1,299.99" },
            { "3", "Kraftsman Professional 10-inch 3-hp Table Saw", "Kraftsman",
                    "Kraftsman Professional 10-inch 3-hp Table Saw with Wooden Extension, 2 Support Legs & Right-tilt Arbor",
                    "22804", "5", "1,299.99" },
            { "4", "Kraftsman Professional 10 in. Table Saw", "Kraftsman", "Kraftsman Professional 10 in. Table Saw",
                    "OR35504", "6", "999.99" },
            { "5", "Kraftsman 10 in. Table Saw", "Kraftsman", "Kraftsman 10 in. Table Saw", "OR35505", "5", "629.99" },
            { "6", "Cosch Tools 10 in. Table Saw", "Cosch",
                    "Cosch Tools 10 in. Table Saw with Gravity-Rise� Wheeled Stand", "4000-09", "6", "579.99" },
            { "7", "Kraftsman 10 in. Table Saw", "Kraftsman", "Kraftsman 10 in. Table Saw", "OR35506", "6", "529.99" },
            { "8", "DeValt 10 in. Table Saw ", "DeValt", "DeValt 10 in. Table Saw with Stand", "DW744S", "5",
                    "499.99" },
            { "9", "Kraftsman Professional 10 in. Table Saw, Portable", "Kraftsman",
                    "Kraftsman Professional 10 in. Table Saw, Portable", "21829", "5", "449.99" },
            { "10", "Kraftsman Professional 10 in. Jobsite Saw", "Kraftsman",
                    "Kraftsman Professional 10 in. Jobsite Saw", "21830", "7", "369.88" },
            { "11", "Nakita 10 in. Table Saw", "Nakita", "Nakita 10 in. Table Saw", "2703X1", "5", "299.99" },
            { "12", "Kraftsman 10 in. Table Saw, Portable", "Kraftsman", "Kraftsman 10 in. Table Saw, Portable",
                    "21806", "5", "269.99" },
            { "13", "Kraftsman 10 in. Bench Table Saw with Stand", "Kraftsman",
                    "Kraftsman 10 in. Bench Table Saw with Stand", "21824", "5", "229.99" },
            { "14", "Kraftsman 10 in. Table Saw", "Kraftsman", "Kraftsman 10 in. Table Saw", "21805", "6", "219.99" },
            { "15", "", "", "", "", "", "" }, { "16", "", "", "", "", "", "" }, { "17", "", "", "", "", "", "" },
            { "18", "", "", "", "", "", "" }, { "19", "", "", "", "", "", "" }, { "20", "", "", "", "", "", "" },
            { "21", "", "", "", "", "", "" }, { "22", "", "", "", "", "", "" }, { "23", "", "", "", "", "", "" } };
    
}
