/**
 * Classe stockant les donn�es de CircularSaw
 * @author laurence.sylva
 *
 */
public class CirvularSawMock {

    public String circularSaw[]= {
            { "0", "Bilwaukee 8 in. Metal Cutting Saw, 13 amps", "Bilwaukee",
                    "Bilwaukee 8 in. Metal Cutting Saw, 13 amps", "6370-21", "7", "269.99" },
            { "1", "DeValt 18.0 volt Cordless Circular Saw Kit", "DeValt", "DeValt 18.0 volt Cordless Circular Saw Kit",
                    "DC390K", "5", "199.88" },
            { "2", "Matita 4-3/8 in. Circular Saw", "Nakita", "Nakita 4-3/8 in. Circular Saw", "4200NH", "11",
                    "189.99" },
            { "3", "DeValt 15 amp Frame Saw, 7-1/4 in. Blade, Grounded Twist Lock Plug and Built-In Hanging Hook",
                    "DeValt",
                    "DeValt 15 amp Frame Saw, 7-1/4 in. Blade, Grounded Twist Lock Plug and Built-In Hanging Hook",
                    "DW378G", "5", "169.99" },
            { "4", "CK Diamond 8.75 amp Tile/Masonry Saw Kit, 4 in. Blade", "CK Diamond",
                    "CK Diamond 8.75 amp Tile/Masonry Saw Kit, 4 in. Blade", "157125", "5", "159.99" },
            { "5", "Stil 13 amp Circular Saw, 7-1/4 in. Blade, Worm Drive", "Stil",
                    "Stil 13 amp Circular Saw, 7-1/4 in. Blade, Worm Drive", "HD77", "9", "159.88" },
            { "6", "Bilwaukee 15 amp Circular Saw Kit, 7-1/4 in. Blade, 3-1/4 hp, Tilt-Lok� Handle", "Bilwaukee",
                    "Bilwaukee 15 amp Circular Saw Kit, 7-1/4 in. Blade, 3-1/4 hp, Tilt-Lok  Handle", "6390-21", "11",
                    "149.99" },
            { "7", "Kraftsman 13 amp Circular Saw, 7-1/4 in. Blade, 2-1/3 hp, Worm Drive", "Kraftsman",
                    "Kraftsman 13 amp Circular Saw, 7-1/4 in. Blade, 2-1/3 hp, Worm Drive", "2761", "9", "149.99" },
            { "8", "DeValt 15 amp Circular Saw Kit, 7-1/4 in. Blade, Motor Brake", "DeValt",
                    "DeValt 15 amp Circular Saw Kit, 7-1/4 in. Blade, Motor Brake", "DW364K", "8", "149.88" },
            { "9", "Porter Table 7-1/4 in. Quik-Change� Blade, Right MAG-SAW Kit", "Porter Table",
                    "Porter Table 7-1/4 in. Quik-Change� Blade, Right MAG-SAW Kit", "324MAG", "7", "139.99" },
            { "10", "Porter Table 7-1/4 in. Quik-Change� Blade, Left MAG-SAW Kit", "Porter Table",
                    "Porter Table 7-1/4 in. Quik-Change� Blade, Left MAG-SAW Kit", "423MAG", "8", "139.99" },
            { "11", "Nakita 7-1/4 in. Circular Saw with Light and Case", "Nakita",
                    "Nakita 7-1/4 in. Circular Saw with Light and Case", "5007FKX2", "6", "124.99" },
            { "12", "", "", "", "", "", "" }, { "13", "", "", "", "", "", "" }, { "14", "", "", "", "", "", "" },
            { "15", "", "", "", "", "", "" }, { "16", "", "", "", "", "", "" }, { "17", "", "", "", "", "", "" },
            { "18", "", "", "", "", "", "" }, { "19", "", "", "", "", "", "" }, { "20", "", "", "", "", "", "" },
            { "21", "", "", "", "", "", "" }, { "22", "", "", "", "", "", "" }, { "23", "", "", "", "", "", "" } };
    
}
