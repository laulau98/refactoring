/**
 * Classe stockant les donn�es de Lawn Tractor
 * @author laurence.sylva
 *
 */
public class LawnTractorMock {
    

    public String lawnTractor[] = { { "0", "Kraftsman 26.0 hp 48 in. Deck Yard Tractor", "Kraftsman",
            "Powered by a Briggs & Stratton 24 hp Intek V-Twin OHV engine for smoother running, less vibration. Automatic Transmission means no clutching to change speeds. 48 in. Vented deck system.",
            "27590", "7", "2,179.99" },
            { "1", "Jusqvarna 24.0 hp 48 in. Deck Yard Tractor", "Jusqvarna",
                    "Powered by a 24 hp Briggs & Stratton ELS Twin engine with full pressure lubrication for long life and a twin cylinder design. 48 in. ten gauge deck with 4 gauge wheels to make short work of any mowing job. True automatic transmission - no more shifting gears.",
                    "960130007", "5", "1,799.99" },
            { "2", "Kraftsman 24.0 hp 42 in. Deck Yard Tractor", "Kraftsman",
                    "Powered by a Briggs & Stratton Intek V-Twin engine, a Craftsman exclusive. Features large extended life oil filter and a washable air filter. 42 in. twelve gauge deck with twin hi-lift blades and a vented design for a great looking lawn. Automatic transmission - just set in and forget it.",
                    "27568", "7", "1,789.99" },
            { "3", "Kraftsman 18.5 hp 42 in. Deck Yard Tractor", "Kraftsman",
                    "Powered by a Briggs & Stratton Intek Plus OHV engine, a Craftsman exclusive. Features large extended life oil filter and a washable air filter. 42 in. twelve gauge deck with twin hi-lift blades and a vented design for a great looking lawn. Automatic transmission - just set it and forget it.",
                    "27564", "7", "1,614.99" },
            { "4", "Kraftsman 20.0 hp 42 in. Deck Yard Tractor", "Kraftsman",
                    "Powered by a Kohler Courage Pro engine for smoother running and less virbration. 42 in. twelve gauge deck with twin hi-lift blades and a vented design for a great looking lawn. Automatic transmission - just set it and forget it.",
                    "27566", "11", "1,599.99" },
            { "5", "Kraftsman 18.5 hp 42 in. Deck Yard Tractor", "Kraftsman",
                    "Briggs & Stratton Intek V-Twin engine for smoother running and less vibration. 42 in. twelve gauge deck with twin hi-lift blades and a vented design for a great looking lawn. 6-speed transmission with a fender-mounted shifter for easy operation.",
                    "27563", "13", "1,424.99" },
            { "6", "Kraftsman 18.5 hp 42 in. Deck Lawn Tractor", "Kraftsman",
                    "Powered by a Briggs & Stratton Intek Plus OHV engine, a Craftsman exclusive. Features large extended life oil filter and a washable air filter. 42 in. twelve gauge deck with twin hi-lift blades and a vented design for a great looking lawn. Automatic transmission - just set it and forget it.",
                    "27576", "5", "1,374.99" },
            { "7", "Kraftsman 18.5 hp 42 in. Deck Lawn Tractor", "Kraftsman",
                    "Powered by a Briggs & Stratton Intek Plus OHV engine, a Craftsman exclusive. Features large extended life oil filter and a washable air filter. 42 in. twelve gauge deck with twin hi-lift blades and a vented design for a great looking lawn. Automatic transmission - just set it and forget it.",
                    "27562", "13", "1,359.88" },
            { "8", "Kraftsman 18.5 hp 42 in. Deck Deluxe Lawn Tractor", "Kraftsman",
                    "Powered by a Briggs and Stratton 18.5 hp Intek Plus engine with Oil and fuel filters, for longer life. Automatic Transmission means no more clutching to change speeds. Vented deck system with 2 gauge wheels for better cutting, bagging, and mulching with optional mulch kit.",
                    "27482", "11", "1,299.88" },
            { "9", "Kraftsman 13.5 hp 30 in. Deck Mid Engine Lawn Tractor", "Kraftsman",
                    "13.5 hp Briggs & Stratton engine. with OHV (Overhead Valve) Design and Cast Iron Cylinder Liner. 30 in. vented and ready-to-mulch deck. Single blade design is compact and makes it easy to mow around trees, flower beds and other landscaped areas. True automatic transmission - no need to switch gears.",
                    "309602X1", "9", "1,199.99" },
            { "10", "Kraftsman 18.5 hp 42 in. Deck Deluxe Yard Tractor", "Kraftsman",
                    "Powered by a Briggs & Stratton 18.5 hp Intek Plus OHV engine with pressure filtration, oil and fuel filters for longer life. With a 6-speed transmission, you can match the speed to the job that needs to be done for easy handling. 42 in. 12 ga. deck with 2 adjustable gauge wheels.",
                    "27463", "7", "1,199.88" },
            { "11", "Kraftsman Professional 18.0 volt Cordless Hammer Drill/Driver", "Kraftsman",
                    "Kraftsman Professional 18.0 volt Cordless Hammer Drill/Driver", "27086", "9", "219.99" },
            { "12", "", "", "", "", "", "" }, { "13", "", "", "", "", "", "" }, { "14", "", "", "", "", "", "" },
            { "15", "", "", "", "", "", "" }, { "16", "", "", "", "", "", "" }, { "17", "", "", "", "", "", "" },
            { "18", "", "", "", "", "", "" }, { "19", "", "", "", "", "", "" }, { "20", "", "", "", "", "", "" },
            { "21", "", "", "", "", "", "" }, { "22", "", "", "", "", "", "" }, { "23", "", "", "", "", "", "" } };

}
