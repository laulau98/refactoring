import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import MockedData.*;

/**
 * 
 * @author laurence.sylva
 *
 *<H1> Refactoring de la classe HadwareStore </H1>
 */


public class HardwareStore extends JFrame
          implements ActionListener {

   private Record recordData;
   private String pData[] []  = new String [ 250 ] [ 7 ];
   private JMenuBar jMenuBar ;
   private JMenu file, view, options, help, tools, about;
   
   private JMenuItem menuItem;
   private MenuHandler menuHandler = new MenuHandler();
   private JTable table;
   private RandomAccessFile randomAccessFile;  
   
   private File file ;
   private JButton cancel, refresh;
   private JPanel buttonPanel ;
   protected boolean validPW = false ;
   private boolean myDebug = false ; 

   private String columnNames[] = {"Record ID", "Type of tool",
       "Brand Name",  "Tool Description", "partNum",
       "Quantity", "Price"} ;

   private DeleteRecord deleteRecord;
   private UpdateRecord updateRecord;
   private Password password;
   private NewRecord newRecord;
   private HardwareStore hwStore;

/**
 *  Utilisation des donn�es mock�es.
 *  les donn�es mock�es sont dans les classes du package MockedData
 */
   private LawnTractorMock lawnTractorMock;
   private LawnMowerMock lawnMowerMock;
   private SanderMock sanderMock;
   private StaplerMock staplerMock;
   private TableSawMock tableSawMock;
   private HammerMock hammerMock;
   private HandDrillMock handDrillMock;
   private DrillPressMock drillPressMock;
   private CirvularSawMock cirvularSawMock;
   private BandSawMock bandSawMock;

 

   private Container c ;
   private int numEntries = 0 , ZERO;

   public HardwareStore()
   {
      super( "Hardware Store: Lawn Mower " );

      recordData  = new Record();
      file = new File( "lawnmower.dat" ) ;
      c = getContentPane() ;

      InitRecord( "lawnmower.dat" ,  lawnMowerMock.lawnMower) ;

      InitRecord( "lawnTractor.dat" , lawnTractorMock.lawnTractor);

      InitRecord( "handDrill.dat" ,  handDrillMock.handDrill) ;

      InitRecord( "drillPress.dat" ,   drillPressMock.drillPress) ;

      InitRecord( "circularSaw.dat" ,   cirvularSawMock.circularSaw) ;

      InitRecord( "hammer.dat" ,   hammerMock.hammer) ;

      InitRecord( "tableSaw.dat" ,  tableSawMock.tableSaw) ;

      InitRecord( "bandSaw.dat" ,  bandSawMock.bandSaw) ;

      InitRecord( "sanders.dat" ,  sanderMock.sanders) ;

      InitRecord( "stapler.dat" ,  staplerMock.stapler) ;
      setup();

      addWindowListener( new WindowHandler( this ) );
      setSize( 700, 400 );
      setVisible( true );
   }
   
   /**
    * 
    * @param menuOption
    * @param menuItems
    * @return a JMenuItem
    */
   public JMenuItem createMenuItem(JMenu menuOption, String... menuItems) {
	   JMenuItem item = new JMenuItem(title);
	   parent.add(item);
	   item.addActionListener(menuHandler);
	   return item;
   }
   
   /**
    * cr�ation du menu Bar en servant de la m�thode pr�c�dente
    * @see createMenuItem()
    */
   public void createMenuBar() {
      createMenuItem(file, "Exit");
      createMenuOptions(view, "Band Saw", "Circular Saw", "Drill Press", "Hammer", "Hand Drill", "Lawn Saw", 
            "Lawn Tractor", "Sander", "Stapler", "Table Saw", "Wet-Dry Vacs", "Storage, Chests & Cabinets");
      createMenuOptions(options, "List All", "Add", "Update", "Delete");
      createMenuOptions(tools, "Debug On", "Debug Off");
      createMenuOptions(help, "Help on HW Store");
      createMenuOptions(about, "About HW Store");
   }


   public void setup()   {
      double loopLimit = 0.0;
      int ii = 0, iii = 0;
      recordData = new Record();
      try {
         randomAccessFile = new RandomAccessFile( "lawnmower.dat" , "rw" );
         file = new File( "lawnmower.dat" ) ;
         numEntries = toArray( randomAccessFile, pData ) ;
         randomAccessFile.close() ;
      }
       catch ( IOException ex ) {
      }
      tableRow(); 
   }

   public void tableRow() {
      table = new JTable(pData, columnNames);
      table.addMouseListener(new MouseClickedHandler(randomAccessFile, table, pData));
      table.setEnabled(true);

      c.add(table, BorderLayout.CENTER);
      c.add(new JScrollPane(table));
      cancel = new JButton("Cancel");
      refresh = new JButton("Refresh");
      buttonPanel = new JPanel();
      buttonPanel.add(cancel);
      c.add(buttonPanel, BorderLayout.SOUTH);

      refresh.addActionListener(this);
      cancel.addActionListener(this);

      updateRecord = new UpdateRecord(hwStore, randomAccessFile, pData, -1);
      deleteRecord = new DeleteRecord(hwStore, randomAccessFile, table, pData);
      password = new Password(this);
   }
   
   /**
    * Re�criture de la m�thode InitRecord 
    * @param fileData
    * @param FileRecord
    */

   public void InitRecord(String fileData, String FileRecord[]) {
      file = new File(fileData);
      sysPrint("initRecord(): 1a - the value of fileData is " + file);
      try {
         sysPrint("initTire(): 1ab - checking to see if " + file + " exist.");
         if (!file.exists()) {
            sysPrint("initTire(): 1b - " + file + " does not exist.");
            randomAccessFile = new RandomAccessFile(file, "rw");
            recordData = new Record();
            for(int i=0; i<FileRecord.length; i++) {
            	switch(FileRecord[i]) {
            	case 0 :
            		recordData.setRecID(Integer.parseInt(FileRecord[i]));
                    sysPrint("initTire(): 1c - The value of record ID is " + recordData.getRecID());
                    break;
            	case 1 : 
            		recordData.setToolType(FileRecord[i]);
                    sysPrint("initTire(): 1cb - The length of ToolType is " + recordData.getToolType().length());
                    break;
            	case 2 :
            		recordData.setBrandName(FileRecord[i]);
            		break;
            	case 3 :
            		recordData.setToolDesc(FileRecord[i]);
                    sysPrint("initTire(): 1cc - The length of ToolDesc is " + recordData.getToolDesc().length());
                    break;
            	case 4 : 
                    recordData.setPartNumber(FileRecord[i]);
                    break;
            	case 5 :
            		recordData.setQuantity(Integer.parseInt(FileRecord[i]));
            		break;
            	case 6 :
            		recordData.setCost(FileRecord[i]);
            		break;
            	default :
            		sysPrint("initTire(): 1d - Calling Record method write() during initialization. " + i);
                    randomAccessFile.seek(ii * Record.getSize());
                    recordData.write(randomAccessFile);	
            	}
            }
         } else {
            sysPrint("initTire(): 1e - " + fileData + " exists.");
            randomAccessFile = new RandomAccessFile(file, "rw");
         }
         randomAccessFile.close();
      } catch (IOException e) {
         System.err.println("InitRecord() " + e.toString() + " " + file);
         System.exit(1);
      }
   }

   /**
    * supression des lignes de conditions (if - else)
    * @param strValue
    * @param title
    */
   public void display(String strValue, String title) {  
         df = new String(strValue);
         file = new File(strValue);
         title = new String(title);
      try {
         sysPrint("display(): 1a - checking to see if " + df + " exists.");
         if (!file.exists()) {
            sysPrint("display(): 1b - " + df + " does not exist.");
         } else {
            randomAccessFile = new RandomAccessFile(df, "rw");
            this.setTitle(title);
            Redisplay(randomAccessFile, pData);
         }
         randomAccessFile.close();
      } catch (IOException e) {
         System.err.println(e.toString());
         System.err.println("Failed in opening " + df);
         System.exit(1);
      }
   }

   /**
    * red�finition de la m�thode Redisplay()
    * @param file
    * @param fileRecord
    */
   public void Redisplay(RandomAccessFile file, String fileRecord) {

      for (int i = 0; i < 7; i++) {
         fileRecord[i] = "";
      }
      int entries = toArray(file, a);
      sysPrint("Redisplay(): 1  - The number of entries is " + entries);
      setEntries(entries);
      c.remove(table);
      table = new JTable(a, columnNames);
      table.setEnabled(true);
      c.add(table, BorderLayout.CENTER);
      c.add(new JScrollPane(table));
      c.validate();
   }

   public void actionPerformed(ActionEvent e) {
      if (e.getSource() == refresh) {
         sysPrint("\nThe Refresh button was pressed. ");
         Container cc = getContentPane();

         table = new JTable(pData, columnNames);
         cc.validate();
      } else if (e.getSource() == cancel)
         cleanup();
   }

   public void cleanup() {
      try {
         randomAccessFile.close();
      } catch (IOException e) {
         System.exit(1);
      }
      setVisible(false);
      System.exit(0);
   }

   public void displayDeleteDialog() {
      sysPrint("The Delete Record Dialog was made visible.\n");
      deleteRecord.setVisible(true);
   }

   public void displayUpdateDialog() {
      sysPrint("The Update Record Dialog was made visible.\n");
      JOptionPane.showMessageDialog(null, "Enter the record ID to be updated and press enter.", "Update Record",
            JOptionPane.INFORMATION_MESSAGE);
      updateRecord = new UpdateRecord(hwStore, randomAccessFile, pData, -1);
      updateRecord.setVisible(true);
   }

   public void displayAddDialog() {
      sysPrint("The New/Add Record Dialog was made visible.\n");
      newRecord = new NewRecord(hwStore, randomAccessFile, table, pData);
      newRecord.setVisible(true);
   }

   public void setEntries(int ent) {
      numEntries = ent;
   }

   public String getPData(int ii, int iii) {
      return pData[ii][iii];
   }

   public int getEntries() {
      return numEntries;
   }

   public void sysPrint(String str) {
      if (myDebug) {
         System.out.println(str);
      }
   }

   public int toArray(RandomAccessFile rAccessFile, String a[][]) {

      Record NodeRef = new Record(), PreviousNode = null;
      int ii = 0, iii = 0, fileSize = 0;

      try {
         fileSize = (int) rAccessFile.length() / Record.getSize();
         sysPrint("toArray(): 1 - The size of the file is " + fileSize);
         if (fileSize > ZERO) {

            NodeRef.setFileLen(rAccessFile.length());

            while (ii < fileSize) {
               sysPrint("toArray(): 2 - NodeRef.getRecID is " + NodeRef.getRecID());

               rAccessFile.seek(0);
               rAccessFile.seek(ii * NodeRef.getSize());
               NodeRef.setFilePos(ii * NodeRef.getSize());
               sysPrint("toArray(): 3 - input data file - Read record " + ii);
               NodeRef.ReadRec(rAccessFile);

               String str2 = a[ii][0];
               sysPrint("toArray(): 4 - the value of a[ ii ] [ 0 ] is " + a[0][0]);

               if (NodeRef.getRecID() != -1) {
                  a[iii][0] = String.valueOf(NodeRef.getRecID());
                  a[iii][1] = NodeRef.getToolType().trim();
                  a[iii][2] = NodeRef.getBrandName().trim();
                  a[iii][3] = NodeRef.getToolDesc().trim();
                  a[iii][4] = NodeRef.getPartNumber().trim();
                  a[iii][5] = String.valueOf(NodeRef.getQuantity());
                  a[iii][6] = NodeRef.getCost().trim();

                  sysPrint("toArray(): 5 - 0- " + a[iii][0] + " 1- " + a[iii][1] + " 2- " + a[iii][2] + " 3- "
                        + a[iii][3] + " 4- " + a[iii][4] + " 5- " + a[iii][5] + " 6- " + a[iii][6]);

                  iii++;

               } else {
                  sysPrint("toArray(): 5a the record ID is " + ii);
               }

               ii++;

            }
         }
      } catch (IOException ex) {
         sysPrint("toArray(): 6 - input data file failure. Index is " + ii + "\nFilesize is " + fileSize);
      }
      return ii;
   }
   public static void main(String args[]) {
      HardwareStore hwstore = new HardwareStore();
      hwstore.hwStore = hwstore;
   }

}

