import java.io.RandomAccessFile;

import javax.swing.JOptionPane;
import javax.swing.JTable;

import java.awt.event.MouseEvent;

import java.awt.event.MouseAdapter;

/**
 * 
 * @author laurence.sylva
 *
 */
public class MouseClickedHandler extends MouseAdapter {
    JTable table;
    String pData[][], columnNames[];
    RandomAccessFile f;
    private HardwareStore hwStore;

    MouseClickedHandler(RandomAccessFile fPassed, JTable tablePassed, String p_Data[][]) {
        table = tablePassed;
        pData = p_Data;
        f = fPassed;

    }
    
    /**
     * @param mouseEvent
     * 
     */
    public void mouseClicked(MouseEvent mouseEvent) {
        if (mouseEvent.getSource() == table) {
            int ii = table.getSelectedRow();
            JOptionPane.showMessageDialog(null, "Enter the record ID to be updated and press enter.", "Update Record",
                    JOptionPane.INFORMATION_MESSAGE);
            UpdateRecord update = new UpdateRecord(hwStore, f, pData, ii);
            if (ii < 250) {
                update.setVisible(true);
                table.repaint();
            }
        }
    }
}
